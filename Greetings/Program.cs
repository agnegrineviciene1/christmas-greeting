﻿using System;
using System.Threading;

class ChristmasGreetings
{
    static void Main()
    {
        int treeHeight = 10;
        DrawChristmasTree(treeHeight);
        Console.WriteLine("\nSu artėjančiomis šventėmis! " +
            "\nVisiems linkiu gerai pailsėti, bet prie stalo neužsisedėti. " +
            "\nLaukia dar ne viena C# pamoka, gavus darbą, gali būt ne kažką :)");
        RunFireworks();
    }

    static void DrawChristmasTree(int height)
    {
        for (int i = 0; i < height; i++)
        {
            Console.Write(new string(' ', height - i));
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine(new string('*', i * 2 + 1));
        }
        Console.ResetColor();
        Console.WriteLine(new string(' ', height) + "| |");
    }

    static void RunFireworks()
    {
        Random rand = new Random();
        string[] fireworks = { "*", "+", "x", ".", "o" };

        for (int i = 0; i < 100; i++)
        {
            Console.SetCursorPosition(rand.Next(Console.WindowWidth), rand.Next(Console.WindowHeight));
            Console.ForegroundColor = (ConsoleColor)rand.Next(1, 16);
            Console.Write(fireworks[rand.Next(fireworks.Length)]);
            Thread.Sleep(100);
        }

        Console.ResetColor();
        Console.SetCursorPosition(0, Console.WindowHeight - 1);
        Console.WriteLine("O jei rimtai, tai lai 2024 m. tampa užsibrėžtų tikslų įgyvendinimo metais! :)");
    }
}
